<?php

namespace Database\Factories;

use App\Models\Station;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Station>
 */
class StationFactory extends Factory
{
    protected $model = Station::class;

    private array $coordinates = [
        [46.568825, 26.916025],
        [47.158455, 27.601442],
        [44.940918, 26.021101],
        [45.657974, 25.601198],
        [44.319305,	23.800678],
        [47.790001,	22.889999],
        [44.933334,	26.033333],
        [46.770439,	23.591423],
        [45.760696,	21.226788],
        [47.163574,	27.582550],
        [44.563889,	27.366112],
        [44.179249,	28.649940],
        [44.439663,	26.096306],
        [47.151726,	27.587914]
    ];

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $coordinates = $this->faker->randomElement($this->coordinates);
        return [
            'name' => $this->faker->colorName,
            'latitude' => $coordinates[0],
            'longitude' => $coordinates[1],
            'company_id' => null,
            'address' => $this->faker->address
        ];
    }
}
