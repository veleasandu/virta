<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Station;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory()
            ->count(1)
            ->create();

        Company::factory()
            ->count(10)
            ->create(['parent_company_id' => null])
            ->each(function ($company) {
                Station::factory()->count(rand(3, 10))->create(
                    ['company_id' => $company->id]
                );

                $id = rand(2, 20);
                Company::factory()
                    ->count($id / 2)
                    ->create(['parent_company_id' => $id])
                    ->each(function ($company) {
                        Station::factory()->count(rand(3, 10))->create(
                            ['company_id' => $company->id]
                        );
                    });
            });
    }
}
