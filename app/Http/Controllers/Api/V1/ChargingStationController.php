<?php

namespace app\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChargingStationRequest;
use App\Http\Services\ChargingStationService;
use Illuminate\Http\JsonResponse;

/**
 * @OA\Info(
 *     version="1.0.0",
 *     title="Virta Api Documentation",
 *     description="Virta Api Documentation",
 *     @OA\Contact(
 *         name="Sandu Velea",
 *         email="veleasandu@gmail.com"
 *     ),
 *     @OA\License(
 *         name="Apache 2.0",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * ),
 * @OA\Server(
 *     url="/api/v1",
 * ),
 */
class ChargingStationController extends Controller
{
    /**
     * @OA\Get(
     *    path="/getChargingStations",
     *    operationId="index",
     *    tags={"ChargingStations"},
     *    summary="Get list of charging stations within the radius of n kilomenters",
     *    description="Get list of charging stations",
     *      @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *            required={"distance", "company_id", "latitude", "longitude"},
     *            @OA\Property(property="distance", type="decimal", format="string", example="25.42"),
     *            @OA\Property(property="company_id", type="integer", format="string", example="12"),
     *            @OA\Property(property="latitude", type="decimal", format="string", example="48.1234"),
     *            @OA\Property(property="longitude", type="decimal", format="string", example="28.1234"),
     *         ),
     *      ),
     *     @OA\Response(
     *          response=200, description="Success",
     *          @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example=""),
     *             @OA\Property(property="data",type="object")
     *          )
     *       )
     */
    public function getChargingStations(ChargingStationRequest $request, ChargingStationService $chargingStationService): JsonResponse
    {
        return response()->json(['data' => collect($chargingStationService->findChargingStations($request))->groupBy('distance')->all()]);
    }
}
