<?php

namespace app\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StationRequest;
use App\Http\Requests\UpdateStationRequest;
use App\Http\Resources\V1\StationCollection;
use App\Http\Resources\V1\StationResource;
use App\Http\Services\StationService;
use App\Models\Station;
use Illuminate\Http\JsonResponse;

class StationController extends Controller
{
    public function index(): StationCollection
    {
        return new StationCollection(Station::paginate());
    }

    public function store(StationRequest $request, StationService $stationService): StationResource
    {
        return new StationResource($stationService->create($request->all()));
    }

    public function show(Station $station): StationResource
    {
        return new StationResource($station);
    }

    public function update(UpdateStationRequest $request, Station $station, StationService $stationService): StationResource|JsonResponse
    {
        return new StationResource($stationService->update($station, $request->all()));
    }

    public function destroy(int $id): JsonResponse
    {
        $station = Station::findOrFail($id);
        $station->delete();

        return response()->json(["data" => [
            "success" => true
        ]]);
    }
}
