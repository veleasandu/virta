<?php

namespace app\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyRequest;
use App\Http\Resources\V1\CompanyCollection;
use App\Http\Resources\V1\CompanyResource;
use App\Http\Services\CompanyService;
use App\Models\Company;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class CompanyController extends Controller
{
    public function index(): CompanyCollection
    {
        return new CompanyCollection(Company::paginate());
    }

    public function store(CompanyRequest $request, CompanyService $companyService): CompanyResource|JsonResponse
    {
        return new CompanyResource($companyService->create($request->all()));
    }

    public function show(Company $company): CompanyResource
    {
        return new CompanyResource($company);
    }

    public function update(CompanyRequest $request, Company $company, CompanyService $companyService): JsonResponse|CompanyResource
    {
        return new CompanyResource($companyService->update($company, $request->all()));
    }

    public function destroy(int $id): JsonResponse
    {
        $company = Company::findOrFail($id);
        $company->delete();

        return response()->json(["data" => [
            "success" => false
        ]]);
    }
}
