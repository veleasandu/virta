<?php

namespace App\Http\Services;

use App\Models\Company;
use App\Models\Station;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class StationService
{
    public function create(array $data): Station
    {
        $company = Company::find($data['company_id']);

        if ($company == null) {
            throw new BadRequestException("Company doesn't exist");
        }

        $station = new Station();

        $station->name = $data['name'];
        $station->company_id = $data['company_id'];
        $station->latitude = $data['latitude'];
        $station->longitude = $data['longitude'];
        $station->address = $data['address'];

        $station->save();

        return $station;
    }

    public function update(Station $station, array $data)
    {
        $company = Company::find($data['company_id']);

        if ($company == null) {
            throw new BadRequestException("Company doesn't exist");
        }

        $station->update($data);

        return $station;
    }
}
