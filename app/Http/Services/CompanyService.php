<?php

namespace App\Http\Services;

use App\Models\Company;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class CompanyService
{
    public function create(array $data): Company
    {
        self::validateParentCompany($data['parent_company_id']);
        $company = new Company();
        $company->name = $data['name'];
        $company->parent_company_id = $data['parent_company_id'];
        $company->save();

        return $company;
    }

    public function update(Company $company, array $data): Company
    {
        self::validateParentCompany($data['parent_company_id']);
        $company->update($data);

        return $company;
    }

    protected static function validateParentCompany(int $parentId): void
    {
        if ($parentId != null && Company::find($parentId) == null) {
            throw new BadRequestException("Parent company doesn't exist");
        }
    }
}
