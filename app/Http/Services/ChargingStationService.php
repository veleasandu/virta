<?php

namespace App\Http\Services;

use App\Http\Requests\ChargingStationRequest;
use Illuminate\Support\Facades\DB;

class ChargingStationService
{
    public function findChargingStations(ChargingStationRequest $request)
    {
        $companyId = intval($request['company_id']);
        $distance = floatval($request['distance']);
        $lat = floatval($request['latitude']);
        $long = floatval($request['longitude']);
        return DB::select(
            "
                    SELECT t.*
                    FROM (SELECT stations.*, (((acos(sin(({$lat}*pi()/180)) * sin((stations.latitude *pi()/180)) + cos(({$lat}*pi()/180)) * cos((stations.latitude *pi()/180)) * cos((({$long} - stations.longitude)*pi()/180)))) * 180/pi()) * 60 * 1.1515) as distance FROM stations WHERE stations.company_id IN (
                                    WITH RECURSIVE rec (id) as
                                    (
                                      SELECT companies.id, companies.name from companies where id={$companyId}
                                      UNION ALL
                                      SELECT companies.id, companies.name from rec, companies where companies.parent_company_id = rec.id
                                    )
                                    SELECT id
                                    FROM rec
                                    )) t WHERE t.distance <= {$distance} ORDER BY t.distance ASC
                            "
        );
    }
}

