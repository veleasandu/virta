<?php

namespace App\Providers;

use App\Http\Services\ChargingStationService;
use App\Http\Services\CompanyService;
use App\Http\Services\StationService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->instance(CompanyService::class, new CompanyService());
        $this->app->instance(StationService::class, new StationService());
        $this->app->instance(ChargingStationService::class, new ChargingStationService());
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
